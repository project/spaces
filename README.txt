
---
Spaces Module **ALPHA** for Drupal 5.x

Authors: Young Hahn, Jeff Miccolis, Alex Barth

---
ABOUT

The spaces module provides a way to control "features" on a group by group basis. Currently we support several features provided by core and contrib modules (book, blog, casetracker) and provide sub modules that implement other features (spaces_shoutbox, spaces_dashboard).

---
INSTALLATION

Spaces module makes a few assumptions about how a site is configured, particularly Organic Groups, some of these have been captured in the 'spaces_conf.inc' file which you can include from your site's settings.php

Spaces also assumes you'll be using notifications/messaging for OG notifications and disables OG's own notifications whenever possible.

Enable the 'Spaces: Navigation' block to see internal group navigation, and 'Spaces: Contextual Tools' to see add content links.